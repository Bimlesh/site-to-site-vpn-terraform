variable "aws_region"  {
  description = "Region for the VPC"
  default = "ap-south-1"
}

variable "vpc_cidr" {
  description = "CIDR for the VPC"
  default = "10.100.0.0/16"
}

variable "public_subnet_cidr" {
  description = "CIDR for the public subnet"
  default = "10.100.0.0/24"
}

variable "private_subnet_cidr" {
  description = "CIDR for the private subnet"
  default = "10.100.1.0/24"
}

variable "ami" {
  description = "Amazon Ubuntu AMI"
  default = "ami-0d2692b6acea72ee6"
}

variable "key_name" {
  description = "Name of the existing key_pair "
  default = "test"
}

variable "destination_cidr" {
  description = "Name of the destination(client) subnet "
  default = "172.31.0.0/16"
}

variable "client_IP" {
  description = "Name of the destination public ip "
  default = "13.59.166.219"
}

variable "instance_type"{
  description = "Instance type u want"
  default = "t2.micro"
}
variable "bgp_asn"{
 description = "bgp_asn u want to provide"
 default = "65000"
}
